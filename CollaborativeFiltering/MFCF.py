import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity


class MF:
    def __init__(self,Y_data,K=2,lam=.1,learning_rate=.5,max_iter=1000,print_every=100,user_based=1 ,X_init=None, W_init=None):
        self.Y_data = Y_data
        self.K = K
        self.learning_rate = learning_rate
        self.max_iter = max_iter
        self.lam = lam
        self.user_based = user_based
        self.print_every = print_every
        self.n_ratings = Y_data.shape[0]


        if X_init == None:
            m = int(np.max(self.Y_data[:,1]))+1
            self.X = np.random.randn(m,K)
        else:
            self.X = X_init
        if W_init == None:
            n = int(np.max(self.Y_data[:, 0])) + 1
            self.W = np.random.randn(K,n)
        else:
            self.W = W_init
        if self.user_based:
            self.n_users = int(np.max(Y_data[:,0],axis = 0).astype(np.int32))+1
            self.n_items = int(np.max(Y_data[:,1],axis = 0).astype(np.int32))+1
        else:
            self.n_users = int(np.max(Y_data[:, 1], axis=0).astype(np.int32))+1
            self.n_items = int(np.max(Y_data[:, 0], axis=0).astype(np.int32))+1


    def normalize_Y(self):
        Y = self.Y_data
        Y_bar = Y.copy().astype(np.float64)
        if self.user_based:
            n_users = int(np.max(Y[:,0],axis = 0).astype(np.int32))+1
            n_items = int(np.max(Y[:,1],axis = 0).astype(np.int32))+1
            col = 0
        else:
            n_users = int(np.max(Y[:, 1], axis=0).astype(np.int32))+1
            n_items = int(np.max(Y[:, 0], axis=0).astype(np.int32))+1
            col = 1

        obj = n_users if self.user_based else n_items
        self.mu = np.zeros((obj,))
        for o in range(obj):
            ids = np.where(Y[:,col]==o)[0].astype(np.int32)
            mean = np.mean(Y[ids,2])
            if np.isnan(mean):
                mean = 0
            self.mu[o] = mean
            Y_bar[ids,2] -= mean

        self.Y_bar = Y_bar

    def loss(self):
        L = 0
        Y_bar = self.Y_bar
        for i in range(Y_bar.shape[0]):
            n,m,rate = int(Y_bar[i,0]),int(Y_bar[i,1]),int(Y_bar[i,2])
            L += (rate-self.X[m,:].dot(self.W[:,n]))
        L/=(2*Y_bar.shape[0])
        # regularization
        L+= self.lam/2*(np.linalg.norm(self.X,'fro')+np.linalg.norm(self.W,'fro'))
        return L

    def get_items_rated_by_user(self, user_id):
        """
        get all items which are rated by user user_id, and the corresponding ratings
        """
        ids = np.where(self.Y_bar[:, 0] == user_id)[0]
        item_ids = self.Y_bar[ids, 1].astype(np.int32)  # indices need to be integers
        ratings = self.Y_bar[ids, 2]
        return (item_ids, ratings)

    def get_users_who_rate_item(self, item_id):
        """
        get all users who rated item item_id and get the corresponding ratings
        """
        ids = np.where(self.Y_bar[:, 1] == item_id)[0]
        user_ids = self.Y_bar[ids, 0].astype(np.int32)
        ratings = self.Y_bar[ids, 2]
        return (user_ids, ratings)

    def updateX(self):

        for m in range(self.n_items):
            user_ids, ratings = self.get_users_who_rate_item(m)
            Wm = self.W[:, user_ids]
            # gradient
            grad_xm = -(ratings - self.X[m, :].dot(Wm)).dot(Wm.T) / self.n_ratings + \
                      self.lam * self.X[m, :]
            self.X[m, :] -= self.learning_rate * grad_xm.reshape((self.K,))

    def updateW(self):
        for n in range(self.n_users):
            item_ids, ratings = self.get_items_rated_by_user(n)
            Xn = self.X[item_ids, :]
            # gradient
            grad_wn = -Xn.T.dot(ratings - Xn.dot(self.W[:, n])) / self.n_ratings + \
                      self.lam * self.W[:, n]
            self.W[:, n] -= self.learning_rate * grad_wn.reshape((self.K,))

    def fit(self):
        self.normalize_Y()
        for it in range(self.max_iter):
            self.updateW()
            self.updateX()
            if it+1 % self.print_every:
                print(f"Loss is {self.loss()} after {(it+1)*self.print_every} iterations")

    def pred(self, u, i):
        """
        predict the rating of user u for item i
        if you need the un
        """
        u = int(u)
        i = int(i)
        if self.user_based:
            bias = self.mu[u]
        else:
            bias = self.mu[i]
        pred = self.X[i, :].dot(self.W[:, u]) + bias
        # truncate if results are out of range [0, 5]
        if pred < 0:
            return 0
        if pred > 5:
            return 5
        return pred

    def pred_for_user(self, user_id):
        """
        predict ratings one user give all unrated items
        """
        ids = np.where(self.Y_bar[:, 0] == user_id)[0]
        items_rated_by_u = self.Y_bar[ids, 1].tolist()

        y_pred = self.X.dot(self.W[:, user_id]) + self.mu[user_id]
        predicted_ratings = []
        for i in range(self.n_items):
            if i not in items_rated_by_u:
                predicted_ratings.append((i, y_pred[i]))

        return predicted_ratings

    def evaluate_RMSE(self, rate_test):
        n_tests = rate_test.shape[0]
        SE = 0  # squared error
        for n in range(n_tests):
            pred = self.pred(rate_test[n, 0], rate_test[n, 1])
            SE += (pred - rate_test[n, 2]) ** 2

        RMSE = np.sqrt(SE / n_tests)
        return RMSE



r_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']

ratings_base = pd.read_csv('../Datasets/ml-100k/ub.base', sep='\t', names=r_cols, encoding='latin-1')
ratings_test = pd.read_csv('../Datasets/ml-100k/ub.test', sep='\t', names=r_cols, encoding='latin-1')

rate_train = ratings_base.as_matrix()
rate_test = ratings_test.as_matrix()

# indices start from 0
rate_train[:, :2] -= 1
rate_test[:, :2] -= 1

rs = MF(rate_train, K = 10, lam = .1, print_every = 10,
    learning_rate = 0.75, max_iter = 50, user_based = 1)
rs.fit()
# evaluate on test data
RMSE = rs.evaluate_RMSE(rate_test)
print('\nUser-based MF, RMSE =', RMSE)











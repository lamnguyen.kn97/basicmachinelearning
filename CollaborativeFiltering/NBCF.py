import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
from scipy import sparse


class CF(object):
    def __init__(self,Y_data,k,dist_func =cosine_similarity,uuCF=1):
        self.uuCF = 1
        self.dist_func = dist_func
        self.Y_data = Y_data if uuCF else Y_data[:,[1,0,2]]
        self.k = k
        self.n_users = int(np.max(Y_data[:,0],axis = 0)) + 1
        self.n_items = int(np.max(Y_data[:,1],axis = 0)) + 1

    def add(self,new_data):
        self.Y_data =  np.concatenate((self.Y_data,new_data),axis = 0)

    def normalize_Y(self):

        Y_norm = self.Y_data.copy().astype(np.float64)
        n_users = self.n_users
        self.mu = np.zeros((n_users))
        for i in range(n_users):
            user_i = np.where(Y_norm[:,0]==i)[0]
            mean = np.mean(Y_norm[user_i,2])
            self.mu[i] = mean
            Y_norm[user_i,2]-=mean

        self.Ybar = sparse.coo_matrix((Y_norm[:, 2],(Y_norm[:, 1], Y_norm[:, 0])), (self.n_items, self.n_users))
        self.Ybar = self.Ybar.tocsr()


    def similarity(self):
        self.S = self.dist_func(self.Ybar.T,self.Ybar.T)

    def fit(self):
        self.normalize_Y()
        self.similarity()


    def _predict(self,u,i):

        votes_i = np.where(self.Y_data[:,1]==i)[0].astype(np.uint8)

        users_i = self.Y_data[votes_i,0].astype(np.uint8)

        sim_users = self.S[u,users_i]
        top_sim = np.argsort(sim_users)[-self.k:]
        r = self.Ybar[i, users_i[top_sim]]


        return r.dot(self.S[u,top_sim])/(r.sum()+1e-8) + self.mu[u]

    def predict(self,u,i):
        if self.uuCF:
            return self._predict(u,i)
        else:
            return self._predict(i,u)



r_cols = ['user_id', 'item_id', 'rating']
ratings = pd.read_csv('ex.dat', sep = ' ', names = r_cols, encoding='latin-1')
Y_data = ratings.as_matrix()
rs = CF(Y_data, k = 2, uuCF = 1)
rs.normalize_Y()
rs.similarity()
print(rs.predict(0,2))


# Test with MovieLens

base_rate = pd.read_csv('../Datasets/ml-100k/ub.base',sep='\t',encoding='latin-1').as_matrix()
test_rate = pd.read_csv('../Datasets/ml-100k/ub.test',sep='\t',encoding='latin-1').as_matrix()

base_rate[:,:2]-=1
test_rate[:,:2]-=1

movie_lens = CF(Y_data=base_rate,k=30,uuCF=1)
movie_lens.fit()

SE = 0

n_tests = test_rate.shape[0]
for _ in test_rate:
    u = _[0]
    i = _[1]
    real = _[2]
    pred = movie_lens.predict(u,i)
    SE += (pred-real)**2

RMSE = np.sqrt(SE/n_tests)
print(RMSE)








from cvxopt import solvers, matrix
"""
Standard Form
x = argmin 1/2 x.T*P*x +q.T*x + r
subject to:
Gx <= h
Ax = b
"""

"""
Problem:
(x,y) = argmin (x-10)**2 + (y-10)**2
subject to:
x+y <= 10
2x+y <= 16 
x+4y <= 32
x>=0
y>=0
"""

P = matrix([[1.,0.],[0.,1.]])
q = matrix([-10.,-10.])
G = matrix([[1.,2.,1.,-1.,0.],[1.,1.,4.,0.,-1.]])
h = matrix([10.,16.,32.,0.,0.])

solvers.options['show_progress'] = False
sol = solvers.qp(P,q,G,h)

print('Solution: ')
print(sol['x'])
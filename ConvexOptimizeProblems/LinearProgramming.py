from cvxopt import matrix,solvers


"""
Standard Form:
x = argmin c.T.dot(x)
subject to: Ax = B
            x>=0
"""


# Problem:
# (x,y) = argmin (-5x -3y)
#     subject to :
#     x+y <=0
#     2x+y <=0
#     x+4y <=0
#     x>=0
#     y>=0


c = matrix([-5.,-3.])
G = matrix([[1.,2.,1.,-1.,0.],[1.,1.,4.,0.,-1.]])
h = matrix([10.,16.,32.,0.,0.])

solvers.options['show_progress'] = False
sol = solvers.lp(c,G,h)
print(sol['x'])
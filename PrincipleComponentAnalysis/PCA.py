import numpy as np
import numpy.linalg as LA

### Generate Data

K = 2
#X = np.random.rand(4,3)
X = np.array([[1,2,0,3],
              [2,0,4,4],
              [4,1,1,2]])
M = X.shape[0]
N = X.shape[1]

# Calculate covariance matrix S

x_mean = np.mean(X,axis = 1)
ones = np.ones((1,N))
X_hat = X - x_mean.reshape((-1,1)).dot(ones)
S = (X_hat.dot(X_hat.T))/N

# Find eigen vectors and lambda

lam, v = LA.eig(S)

print(lam)
print(v)

ids = np.argsort(lam)[-K:][::-1]


# Calculate U_k by getting K-largest eigen vector from S
U_k = v[:,ids]

# X = U_k.dot(Z) + U_k_hat.dot(Y) (Y: bias) => X appro U_k.dot(Z)
Z = U_k.T.dot(X_hat)

X_new = U_k.dot(Z)

print(X)
print(X_new)


# Test  X = X_new + x_mean.dot(ones.T)
print(X_new[:,1]+x_mean)









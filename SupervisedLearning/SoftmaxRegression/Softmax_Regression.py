import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
from sklearn import linear_model

N = 100
C = 3
np.random.seed(1)

## Random data
means = np.array([[1,2],[5,-6],[-5,-7]])

cov = np.array([[1,0],[0,1]])

X1 = np.random.multivariate_normal(means[0],cov,N).T
X2 = np.random.multivariate_normal(means[1],cov,N).T
X3 = np.random.multivariate_normal(means[2],cov,N).T


y = [0]*N + [1]*N + [2]*N
y = np.array(y).reshape((C*N,1))

## Visualize data


plt.plot(X1[0,:],X1[1,:],'rx',markersize =4)
plt.plot(X2[0,:],X2[1,:],'go',markersize =4)
plt.plot(X3[0,:],X3[1,:],'b.',markersize =4)


plt.show()

## Convert y to one-hot matrix

Y = np.zeros((C*N,C))
for i in range(C*N):
    k = y[i]
    Y[i,k] = 1


## Train

X = np.concatenate((X1,X2,X3),axis = 1)
X = np.concatenate((np.ones((1,X.shape[1])),X),axis =0)

d = X.shape[0]
def softmax(Z):
    """
    Compute softmax values for each sets of scores in Z.
    each column of Z is a set of score.
    """
    e_Z = np.exp(Z - np.max(Z, axis = 0, keepdims = True))
    A = e_Z / e_Z.sum(axis = 0)
    return A

def softmax_regression(W_init, eta, max_count = 10000):
    W = [W_init]
    w_pre = W_init
    count = 0
    check = 20
    for _ in range(max_count):
        suffle_idx = np.random.permutation(3*N)
        for i in suffle_idx:
            xi = X[:,i].reshape((d,1))
            yi = Y[i,:].reshape((C,1))
            ai = softmax(W[-1].T.dot(xi))
            w = W[-1] + eta*(xi.dot((yi-ai).T))
            count += 1
            W.append(w)
            if count % check == 0:
                if np.linalg.norm(w-w_pre) < 1e-4:
                    return W
                w_pre = w
    return W

W_init = np.random.rand(d,C)
W = softmax_regression(W_init,0.05)
print(W[-1])

Y_pred = softmax(W[-1].T.dot(X)).T

y_pred = np.argmax(Y_pred,axis = 1)

print(accuracy_score(y,y_pred))

### Test by Sklearn ####

X_train = X[:,:3*N-10].reshape((3*N-10,d))
y_train = y[:3*N-10]

X_test = X[:,3*N-10:].reshape((10,d))
y_test = y[3*N-10:]

log_reg = linear_model.LogisticRegression(C=10e5,solver = 'lbfgs',fit_intercept=False,multi_class='multinomial')
log_reg.fit(X_train,y_train)

y_pred = log_reg.predict(X_test)
print(accuracy_score(y_test,y_pred))
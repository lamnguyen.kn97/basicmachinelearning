import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model

# height (cm)
X = np.array([[147, 150, 153, 158, 163, 165, 168, 170, 173, 175, 178, 180, 183]]).T
# weight (kg)
y = np.array([[49, 50, 51,  54, 58, 59, 60, 62, 63, 64, 66, 67, 68]]).T

# draw data
plt.plot(X,y,'bx',markersize = 4)
plt.xlabel('height')
plt.ylabel('weight')


# solve for weight
m = X.shape[0]
X_bar = np.concatenate((np.ones((m,1)),X),axis = 1)

# W = (X_bar.T*Xbar)-1*X_bar.T*y

A = np.linalg.pinv(X_bar.T.dot(X_bar))
W = (A.dot(X_bar.T)).dot(y)
print(W)

# visualize solution

x_A = X_bar[0]
y_A = x_A.dot(W)

x_B = X_bar[-1]
y_B = x_B.dot(W)

plt.plot([x_A,x_B],[y_A,y_B],label = "Solution")
plt.show()

# using sklearn

linear_reg = linear_model.LinearRegression(fit_intercept=False)
linear_reg.fit(X_bar,y)
W_sk = linear_reg.coef_
print(W_sk)








from __future__ import division, print_function, unicode_literals
import math
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score

N = 100 # number of points per class
d0 = 2 # dimensionality
C = 3 # number of classes
X = np.zeros((d0, N*C)) # data matrix (each row = single example)
y = np.zeros(N*C, dtype='uint8') # class labels

for j in range(C):
  ix = range(N*j,N*(j+1))
  r = np.linspace(0.0,1,N) # radius
  t = np.linspace(j*4,(j+1)*4,N) + np.random.randn(N)*0.2 # theta
  X[:,ix] = np.c_[r*np.sin(t), r*np.cos(t)].T
  y[ix] = j
# lets visualize the data:
# plt.scatter(X[:N, 0], X[:N, 1], c=y[:N], s=40, cmap=plt.cm.Spectral)

plt.plot(X[0, :N], X[1, :N], 'bs', markersize = 7);
plt.plot(X[0, N:2*N], X[1, N:2*N], 'ro', markersize = 7);
plt.plot(X[0, 2*N:], X[1, 2*N:], 'g^', markersize = 7);
# plt.axis('off')
plt.xlim([-1.5, 1.5])
plt.ylim([-1.5, 1.5])
cur_axes = plt.gca()
cur_axes.axes.get_xaxis().set_ticks([])
cur_axes.axes.get_yaxis().set_ticks([])

plt.savefig('EX.png', bbox_inches='tight', dpi = 600)
plt.show()

#### Train multilayer perceptron
print(X.shape)

def convert_label(y,C):
    Y = np.zeros((C,X.shape[1]))
    for j in range(C):
        idx = range(N*j,N*(j+1))
        Y[j,idx] = 1
    return Y


def softmax(Z):
    """
    Compute softmax values for each sets of scores in Z.
    each column of Z is a set of score.
    """
    e_Z = np.exp(Z - np.max(Z, axis = 0, keepdims = True))
    A = e_Z / e_Z.sum(axis = 0)
    return A

def MultiLayer(eta = 1,max_count = 100000):

    N = X.shape[1]
    d0 = 2
    d1 = 100
    d2 = C
    Y = convert_label(y,C=3)
    W1 = np.random.rand(d0,d1)
    b1 = np.zeros((d1,1))
    W2 = np.random.rand(d1,d2)
    b2 = np.zeros((d2,1))
    for _ in range(max_count):
        # feed forward
        Z1 = W1.T.dot(X) + b1
        A1 = np.maximum(Z1,0)
        Z2 = W2.T.dot(A1) + b2
        Y_hat = softmax(Z2)

        # back propagation

        E2 = (Y_hat-Y)/N
        dW2 = A1.dot(E2.T)
        db2 = np.sum(E2,axis = 1, keepdims = True)

        E1 = W2.dot(E2)
        E1[Z1<=0] = 0
        dW1 = X.dot(E1.T)
        db1 = np.sum(E1,axis = 1, keepdims = True)

        # update
        W1 += -eta * dW1
        b1 += -eta * db1
        W2 += -eta * dW2
        b2 += -eta * db2


    return W1,b1,W2,b2


W1,b1,W2,b2 = MultiLayer()

Z1 = W1.T.dot(X) + b1
A1 = np.maximum(Z1,0)
Z2 = softmax(W2.T.dot(A1) + b2)

y_pred = np.argmax(Z2,axis = 0)



print(f"Accuracy of MLP algorithm is {100*np.mean(y==y_pred)}%")









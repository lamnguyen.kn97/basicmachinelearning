import numpy as np
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from cvxopt import solvers,matrix


np.random.seed(21)
### Generate Data
N = 20

means = [[2,2],[4,1]]
cov = [[0.3,0.2],[0.2,0.3]]

X1 = np.random.multivariate_normal(means[0],cov,N)
X2 = np.random.multivariate_normal(means[1],cov,N)

X1[-1,:] = [4,2]

X = np.concatenate((X1.T,X2.T),axis = 1)
y = np.concatenate((np.ones((1,N)),-np.ones((1,N))),axis = 1)

### Visualize data

plt.plot(X[0,y.flatten() == 1],X[1,y.flatten() == 1],'rx')
plt.plot(X[0,y.flatten() == -1],X[1,y.flatten()== -1],'bs')

plt.show()


### Solve soft SVM by sklearn

C = 100

clf = SVC(kernel ='linear',C=C)
clf.fit(X.T,y.T)

w_sklearn = clf.coef_
b_sklearn = clf.intercept_[0]

print(w_sklearn)
print(b_sklearn)

### Solve by cvxopt

V = np.concatenate((X1.T,-(X2.T)),axis = 1)
P = matrix(V.T.dot(V))
q = matrix(-np.ones((2*N,1)))

A = matrix(y)
b = matrix(np.zeros((1,1)))

G = matrix(np.vstack((-np.eye(2*N),np.eye(2*N))))
h = matrix(np.vstack((np.zeros((2*N,1)),C*np.ones((2*N,1)))))

solvers.options['show_progress'] = False
sol = solvers.qp(P,q,G,h,A,b)

l = sol['x']
l = np.array(l)


S = np.where(l > 1e-5)[0] # support set
S2 = np.where(l < .999*C)[0]

M = [val for val in S if val in S2] # intersection of two lists

XT = X # we need each column to be one data point in this alg
VS = V[:, S]
lS = l[S]
yM = y.T[M]
XM = XT[:, M]

w_dual = VS.dot(lS).reshape(-1, 1)
b_dual = np.mean(yM.T - w_dual.T.dot(XM))
print(w_dual.T, b_dual)



### Gradient Descent

X1_ = np.concatenate((X1.T,np.ones((1,N))),axis = 0)
X2_ = np.concatenate((X2.T,np.ones((1,N))),axis = 0)

Z = np.concatenate((X1_,-X2_),axis = 1)

w = np.random.rand(3,1)

lam = 1/C
eta = 0.001

for _ in range(100000):

    u = w.T.dot(Z)
    H = np.where(u < 1)[1]
    grad = -np.sum(Z[:,H],axis = 1).reshape((-1,1)) + lam*w
    grad[-1] -= lam*w[-1]
    w -= eta*grad


print(w)


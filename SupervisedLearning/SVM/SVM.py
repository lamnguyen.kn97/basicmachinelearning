import numpy as np
import matplotlib.pyplot as plt
from cvxopt import matrix,solvers
from sklearn.svm import SVC


## Generate Data
means = [[2,2],[4,2]]
cov = [[.3,.2],[.2,.3]]
N = 20

X1 = np.random.multivariate_normal(means[0],cov,N)
X2 = np.random.multivariate_normal(means[1],cov,N)

X = np.concatenate((X1.T,X2.T),axis = 1)
y = np.concatenate((np.ones((1,N)),-np.ones((1,N))),axis = 1)


## Visualize data
plt.plot(X[0,y.flatten()==1],X[1,y.flatten()==1], 'rs')
plt.plot(X[0,y.flatten()==-1],X[1,y.flatten()==-1], 'gp')




## Optimize problem by cvxopt

V = np.concatenate((X1.T, -X2.T), axis = 1)
K = matrix(V.T.dot(V)) # see definition of V, K near eq (8)

p = matrix(-np.ones((2*N, 1))) # all-one vector
# build A, b, G, h
G = matrix(-np.eye(2*N)) # for all lambda_n >= 0
h = matrix(np.zeros((2*N, 1)))
A = matrix(y) # the equality constrain is actually y^T lambda = 0
b = matrix(np.zeros((1, 1)))
solvers.options['show_progress'] = False
sol = solvers.qp(K, p, G, h, A, b)

l = np.array(sol['x'])
print('lambda = ')
print(l.T)

# Find support vectors

epsilon = 1e-6 # just a small number, greater than 1e-9
S = np.where(l > epsilon)[0]

VS = V[:, S]
XS = X[:, S]
yS = y[:, S]
lS = l[S]
# Calculate w and b
w = VS.dot(lS)
b = np.mean(yS.T - w.T.dot(XS))

print('w = ', w.T)
print('b = ', b)

# Visualize result
x1 = 1
x2 = 5

y1 = -(w[0][0]*x1 + b)/(w[1][0])
y2 = -(w[0][0]*x2 + b)/(w[1][0])
plt.plot(np.array([x1,x2]),np.array([y1,y2]))
plt.show()


### Use sklearn

y1 = y.reshape((2*N,))
X1 = X.T # each sample is one row
clf = SVC(kernel = 'linear', C = 1e5) # just a big number

clf.fit(X1, y1)

w = clf.coef_
b = clf.intercept_
print('w = ', w)
print('b = ', b)



import numpy as np
import matplotlib.pyplot as plt

# generate data
N = 1000

means = np.array([[1,1], [5,6]])
cov_matrix = np.array([[1,0],[0,1]])

X1 = np.random.multivariate_normal(means[0],cov_matrix,N)
X2 = np.random.multivariate_normal(means[1],cov_matrix,N)

X = np.concatenate((X1,X2),axis = 0)
X = X.T

y = np.array([-1]*N + [1]*N)
#y = y.reshape((2*N,1))


# visualize data

plt.plot(X[0,y==-1],X[1,y==-1],'rx',markersize = 4,label = 'Type 1')
plt.plot(X[0,y==1],X[1,y==1],'go',markersize = 4, label = 'Type 2')
plt.legend()

# add bias to X

m = X.shape[1]
X_bar = np.concatenate((np.ones((1,m)),X),axis = 0)


# solve w for X
# J(w,xi,yi) = -yi.w.T.xi
# dJ(w,xi,yi)/d(w) = -yi.xi
N = X_bar.shape[1]

def h(w,x):
    return np.sign(w.T.dot(x))

def grad(i):
    return -y[i]*X_bar[:,i]

def is_converged(w):
    return np.array_equal(h(w,X_bar),y)


def pla(w_init, eta, max_count = 10000):
    W = [w_init]
    w_pre = w_init
    check_grad = 20
    count = 0
    d = X_bar.shape[0]
    for _ in range(max_count):
        suffle_idx = np.random.permutation(N)
        for i in suffle_idx:
            xi = X_bar[:,i].reshape((d,1))
            yi = y[i]
            if h(W[-1],xi)[0] != yi:
                w = W[-1] + yi*xi
                W.append(w)
                count+=1

        if is_converged(W[-1]):
            return W

    return W

w_init = np.random.randn(X_bar.shape[0],1)
eta = 1
W = pla(w_init,eta)
print(W[-1])
w = W[-1]
x_A = 0
y_A = -(w[0][0] + w[1][0]*x_A)/w[2][0]

x_B = 10
y_B = -(w[0][0] + w[1][0]*x_B)/w[2][0]
plt.plot([x_A,x_B],[y_A,y_B])
plt.show()







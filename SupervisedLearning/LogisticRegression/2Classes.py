import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from sklearn import linear_model
from sklearn.metrics import accuracy_score
np.random.seed(2)

X = np.array([[0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 1.75, 2.00, 2.25, 2.50,
              2.75, 3.00, 3.25, 3.50, 4.00, 4.25, 4.50, 4.75, 5.00, 5.50]])
y = np.array([0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1])

x = deepcopy(X)

# draw data

x_0 = X[:,y==0].flatten()
x_1 = X[:,y==1].flatten()

plt.plot(x_0,y[y==0],'ro',label='fail')
plt.plot(x_1,y[y==1].flatten(),'bx',label = 'pass')
#plt.show()

# extended data

X = np.concatenate((np.ones((1, X.shape[1])), X), axis = 0)

# Solve for the weight
# Lost function J(wi,xi,yi) = yilog(ai) + (1-yi)log(1-ai)
# ai = sigmoid(w.T*xi)
# zi = w.T*xi
# dJ(w,xi,yi)/dw = (zi - yi)*xi

N = X.shape[1]
d = X.shape[0]

def sigmoid(X):
    return 1/ (1+np.exp(1)**(-X))

def logistic_regression(w_init, eta, max_count = 10000):
    W = [w_init]
    w_pre = w_init
    count = 0
    check = 10
    for _ in range(max_count):
        suffle_idx = np.random.permutation(N)
        for i in suffle_idx:
            xi = X[:,i].reshape((d,1))
            yi = y[i]
            zi = sigmoid(W[-1].T.dot(xi))
            w = W[-1] - eta*((zi-yi)*(xi))
            count += 1
            W.append(w)
            if count % check == 0:
                if np.linalg.norm(w-w_pre) < 1e-3:
                    return W
                w_pre = w
    return W


w_init = np.random.randn(X.shape[0],1)
eta = 0.1

W = logistic_regression(w_init,eta)
print(W[-1])
w = W[-1]

xx = np.linspace(0, 6, 1000)
w0 = w[0][0]
w1 = w[1][0]
threshold = -w0/w1
yy = sigmoid(w0 + w1*xx)
plt.axis([-2, 8, -1, 2])
plt.plot(xx, yy, 'g-', linewidth = 2)
plt.plot(threshold, .5, 'y^', markersize = 8)
plt.xlabel('studying hours')
plt.ylabel('predicted probability of pass')
plt.show()

#### Scikit learn ####

X_train = X.reshape((N,d))[:N-3,:]
y_train = y[:N-3]

x_test = X.reshape((N,d))[N-3:,:]
y_test = y[N-3:]
log_reg = linear_model.LogisticRegression(C=1e5,fit_intercept=False)

log_reg.fit(X_train,y_train)
print(log_reg.coef_)
y_pred = log_reg.predict(x_test)

print(f"Accuracy between predict and real data is {100*accuracy_score(y_test,y_pred)} %")
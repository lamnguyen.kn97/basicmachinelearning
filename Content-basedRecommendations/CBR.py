import pandas as pd
import numpy as np
# Number of users
u_cols = ['user_id','age','sex','occupation','zip_code']
users = pd.read_csv('./datasets/ml-100k/u.user',sep='|',names=u_cols,
                    encoding = 'latin-1')
n_users = users.shape[0]
print(f'Number of users: {n_users}')

# Rating file

r_cols = ['user_id','movie_id','rating','unix_timestamp']

rating_base = pd.read_csv('./datasets/ml-100k/ua.base',sep='\t',names=r_cols,
                          encoding = 'latin-1')
rating_test = pd.read_csv('./datasets/ml-100k/ua.test',sep='\t',names=r_cols,
                          encoding = 'latin-1')

rate_train = rating_base.as_matrix()
rate_test = rating_test.as_matrix()



# Items file

i_cols = ['movie id', 'movie title' ,'release date','video release date', 'IMDb URL', 'unknown', 'Action', 'Adventure',
 'Animation', 'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy',
 'Film-Noir', 'Horror', 'Musical', 'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western']

items = pd.read_csv('../Datasets/ml-100k/u.item', sep='|', names=i_cols,
 encoding='latin-1')

n_items = items.shape[0]

X0 = items.as_matrix()
X_train_counts = X0[:,-19:]

#tfidf

from sklearn.feature_extraction.text import TfidfTransformer
transformer = TfidfTransformer(smooth_idf = True, norm = 'l2')
tfidf = transformer.fit_transform(X_train_counts.tolist()).toarray()


# Train

def get_items_rated_by_user(rate_matrix,user_id):
    y = rate_matrix[:,0]
    ids = np.where(y==user_id)[0]
    item_ids = rate_matrix[ids,1] - 1
    scores = rate_matrix[ids,2]
    return item_ids,scores

from sklearn.linear_model import Ridge
from sklearn import linear_model

d = tfidf.shape[1]
W = np.zeros((d,n_users))
b = np.zeros((1,n_users))

for n in range(n_users):
    ids,scores = get_items_rated_by_user(rate_train,n)
    clf = Ridge(alpha = 0.01, fit_intercept = True)
    X_hat = tfidf[ids,:]

    clf.fit(X_hat,scores)
    W[:,n] = clf.coef_
    b[0,n] = clf.intercept_







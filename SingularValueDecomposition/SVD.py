import numpy as np
import numpy.linalg as LA

np.random.seed(11)
m,n = 3,2
A = np.zeros((m,n))
A = np.array([[
    1,2,3],[4,5,6],[7,8,9]])

U,S,V = LA.svd(A)

print("Fro norm of U.UT and I: ")
print(LA.norm(U.dot(U.T)-np.eye(3)))
print("Fro norm of V.VT and V: ")
print(LA.norm(V.dot(V.T)-np.eye(3)))

## || A - Ak||f = Sk+1 + Sk+2 + .... Sn

print("Fro norm of A: ")
print(LA.norm(A,'fro')**2-np.sum(S**2))

